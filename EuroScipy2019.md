#EuroSciPy 2019
## Tutorial day 1

### Image processing in SKImage
```
conda env create -f environment.yml 
conda activate euroscipy2019_skimage
```


### Reproducible DataScience

Data repos

- [Zenodo](https://zenodo.org)
- [FigShare](https://figshare.com)
- [Dataverse](https://dataverse.org)


Gitter channel: https://gitter.im/SwissDataScienceCenter/renku

[Papermill](https://papermill.readthedocs.io/en/latest/index.html) Docs: https://papermill.readthedocs.io/en/latest/usage-parameterize.html

### Airflow

bit.ly/euroscipy-airflow

### Quantum Computing with QuTIP
Geared towards real QM (= spin)-related computation (i.e., not like DWave)

## Tutorial day 2

### Valerio : Advanced NumPy

https://github.com/leriomaggio/numpy-euroscipy

Fancy indexing (with boolean mask) only in numpy, not lists?


```
docker exec -it mycompss /bin/bash
```

```
/etc/init.d/compss-monitor start
```
```
jupyter-notebook --no-browser --allow-root --ip=172.17.0.2 --NotebookApp.token=
```
URLs

- http://localhost:8888
- http://localhost:8080/compss-monitor/index.zul

### kCSD
to make local package available

```
!pip install scipy
import sys
sys.path.append("/Users/puetz/Work/EuroSciPy2019/kCSD-python")
```

---
## Day 1
### Keynote: Samuel Farrens - Image processing, from galaxies to brains

#### Cosmology
discoveries

- expanding universe
- dark matter
- microwave background
- accelerated expansion - dark energy?
- gravitational waves

Remove PSF (point spread function)
Farrens et al 2017
(https://github.com/CosmoStat/sf_deconvolve)

PSF super-resolution
Schmitz et al ??

##### DL

#### Biomedical Imaging

faster acquisition: compressed sensing w/ SPARKLING

- http://cosmic.comstat.  ???
- https://github.com/cea-cosmic
- https://github.com/cea-cosmic/pysap
- https://github.com/cea-cosmic/ModOpt


### Zac Hatfields-Dodds: Testing
@Zac-HD

→ hypothesis package

### Sara Diot-Girard: What about tests in Machine Learning projects?


Github: @SdgJlbl - talk will be on GH

Reference testing: ```tdda``` module (test-diven data analysis)


### Nick: Sci DevOps
slides.app.goo.gl/KjgDT

https://github.com/nickdelgrosso/devops_talk_euroscipy2019


### VOILA

- req file
- Procfile


https://github.com/maartenbreddels/ipymaterialui

voila-gallery.org
https://github.com/voila-gallery/gallery
https://github.com/QuantStack/voila
voila.rtfd.io
quantstack.net


### transonic

https://tiny.cc/euroscipy2019-transonic


### Jupyter best practices
http://koenigsweg.com

jupytext

docker/binder

journals ...

### Lena Oden: Lessons learned from comparing Numba-CUDA and C-CUDA

numba

---
## Day 2

### Intel Keynote
software.intel.com/distribution-for-python


### Valerio Maggio: KubeFlow Kale
https://github.com/kubeflow/kubeflow

![Twitter snippets](Photos/IMG_1844.JPG)
![Kubernetes](Photos/IMG_1845r.JPG)
![Distributed DL Training](Photos/IMG_1852r.JPG)

ML in cloud 

- Kubeflow
- Polyaxon

(both based on Kubernetes), cloud agnostic (also on premise)
Orchestration geared for Docker, but can be any container
![AI HHub. and Kubeflow Intro](Photos/IMG_1853r.jpeg)
![screen shot](Photos/IMG_1952r.jpeg)

![Kubeflow Pythhon SDK](Photos/IMG_2029r.jpeg)
![Notebook -> Kubeflow pipeline 1](Photos/IMG_2030r.jpeg)
![Notebook -> Kubeflow pipeline 2](Photos/IMG_2031r.jpeg)
![Kale main components](Photos/IMG_2046r.jpeg)
![static analysis - marshal](Photos/IMG_2047r.jpeg)




Custom components via API


#### Kubeflow Pipelines
https://github.com/kubeflow/pipelines

- experiment
- deployment
- ...

ML-pipeline

- multi-step ML
- Python SDK
- Jupyter NB integration

#### Kubeflow Automated PipeLines Engine
https://github.com/kubeflow-kale/jupyterlab-kubeflow-kale

aimed at data scientists

Jupyter → KALE → KubeFlow

- ... 
- (other tools)
- ...
- Jinja

https://github.com/kubeflow-kale/examples

→ [Papermill](https://papermill.readthedocs.io/en/latest/index.html) for HP-tuning

→ [RedisAI](https://oss.redislabs.com/redisai/)
![RedisAI sketch](Photos/IMG_2048r.jpeg)


### Andrii Gakhov: Probabilistic Data Structures and Algorithms (PDSA)

useful when not necessarily correct/exakt answer is needed

Ecosystem (Photo)

- q-quantile estimation
- approx. number of distinct elements

![Big Data Ecosystem](Photos/IMG_2050r.jpeg)

#### Examples
##### Frequency

- most trending tags on twitter (\\(\sim5\times10^8/d\\))

trad. approach: hashtable (lin in memory)

-  one-pass!
-  high freq throughput

**Count-Min Sketch**

- hashing

![Count-Min Sketch 1](Photos/IMG_2051r.jpeg)
![Count-Min Sketch 2](Photos/IMG_2052r.jpeg)
![Count-Min Sketch 3](Photos/IMG_2053r.jpeg)

in `pdsa` library

##### Count: HyperLogLog

e.g., visitors @ ebay, followers @ Twitter
![HyperLogLog 1](Photos/IMG_2057r.jpeg)

![HyperLogLog 2](Photos/IMG_2058r.jpeg)

also in `pdsa` library

readily available in [REDIS](https://redis.io) ([commercial](https://redislabs.com)?)

![Final Notes](Photos/IMG_2059r.jpeg)

### Francesco Pierfederici: 30m Telescope

### Francesco Bonazzi: Matrix calculus with SymPy
https://www.sympy.org/en/index.html

Symbolic Calculations in Python


### Keynote Sara Issaoun: black hole
https://dias.ie/yerac2019/Presentations/IssaounTalk.pdf

### Valentin Haenel: Understanding Numba

- compiler (functions), numerically focussed
- type specialized
- JIT
- LLVM as compiler backend (`llvmlite` on Windows, comes with package)

#### Tipps

- always JIT (`njit`, see photo below)
- numpy arrays
- typed containers
- small functions (→ inline)
- `for` loops fine

all reasonable OSs supported
![decorrator example](Photos/IMG_2064r.jpeg)

https://numba.pydata.org

### Ronan Lamy: PyPy meets SciPy
https://https://pypy.org

https://github.com/antocuni/pypy-wheels

- pythonic code
- abstraction for free
- Python as 1\\( ^{st} \\) class HPC language

#### Issues

- lags behind CPython
- no conda

new C API

https://github.com/pyhandle/hpy

### Javier Álvarez: High performance machine learning with `dislib`
htttps://dislib.bsc.es

photo: "most beautiful Supercomputer" (inside church)

distributed version of scikit-learn

- build on `PyCOMPSs`
    - sequential programming programming
    - task-based programming model
    - infrastructure-agnostic (clusters, containers, clouds)
    - automated parallelization
- distributed data representation
- scikit-learn interface
- transparent parallelism


Alternatives

- dask-ml
- spark ML

![dislib vs. scikit learn](Photos/IMG_2066r.jpeg)

### Juan Luis Cano Rodríguez: Can we make Python fast without sacrificing readability? 

numba for Astrodynamics


### Yaman Güçlü (MPI Plasma Physics): PsyDac

SymPDE

![SymPDE 1](Photos/IMG_2067r.jpeg)
![SymPDE 2](Photos/IMG_2068r.jpeg)
![SymPDE 3](Photos/IMG_2069r.jpeg)

### Lightning Talks

#### travelling salesmann with ???
![Bear route](Photos/IMG_2071r.jpeg)

#### Oliver: easyagents
https://github.com/djcordhose
![Alt Image Text](Photos/IMG_2072r.jpeg)


#### pycon.de
2019 sold out, next chance:
9.–11. Oct 2020 in Berlin

#### Vasily Litvinov (Intel): Profiling Python + C for Fun
Take home message: **Measure, don't guess!**

- event-based
    - built-in
- instumentation-based
    - ```line_profiler```
- statistical
    - ```vmprof```
    - ```statprof```
    - ```pysamprof``` (open-sourced)

```vtune``` free, sign up

#### Alexander Makaryev: DAAL4Py (Intel)
https://github.com/IntelPython/daal4py


#### comparison benchmarks

https://github.com/pleiszenburg/gravitation

#### Formula 1
- more interesting with different scoring scheme?
    - how to motivate more take over manoeuvres 
- suggestions
    - current 
    - winner takes all
    - linear decrease from first rank
    - ...
- simple evaluation through 
     - simulation
     - testing on historical data ((*how?*))

#### Data-Object for Multi-physics computation
- http://www.pyleecan.org
- https://github.com/Eomys/py

#### distributing irregular tasks  over network
home-made queueing system

#### Loîs: dask-jobqueue
transition from labtop to cluster

*Transform your supercomputer into an interactive machine*

→ `dask` library
![dask screen shot](Photos/IMG_2073r.jpeg)


#### Geir Arne Hjelle: test your code on Python 3.8
Release Oct. 21, last beta out now

```
docker run -it --rm python:rc
```
for `pip install`:

```
docker run -it --rm python:rc /bin/bash
import parse
```
Dockerfile

```
from python:rc
RUN pythhon -m pip install parse
```

#### Matteo Schiavinato: Plant speed dating
Phylogeny on tobacco

#### Petar Mlinaric: Model order reduction (MOR)
[MPI Magdeburg]

`pyMOR` package
https://github.com/pyMOR/pyMOR

DFG-funding: "Research Software Sustainability"

#### Valentin Haenel: Numba integration testing
- https://github.com/numba/numba-integration-testting
- https://github.com/numba/texasbbq

#### quick intro to pandas logo
Juan

#### pyntxos
bit.ly/pyntxos2019
https://github.com/python-sprints/pyntxos

bar hopping



Sprints

- scipy lecture notes



### EuroScipy 2020, July 27–31
